### Theory
 Theory:-<br>
(1)  A Low Pass Filter is a circuit that can be easily pass the frequencies below its cut-off frequency and attenuates all other frequencies.<br>

(2) It is a circuit that can be designed to modify, reshape or reject all unwanted high frequencies of an electrical signal.<br>

(3) In other words they “filter-out” unwanted signals and an ideal filter will separate and pass sinusoidal input signals based upon their frequency. <br>

(4) In low frequency applications (up to 100kHz), passive filters are generally constructed using simple RC (Resistor-Capacitor) networks.<br>

(5) The frequency range “below” this cut-off point ƒC is generally known as the Pass Band as the input signal is allowed to pass through the filter.<br>

(6) The frequency range “above” this cut-off point is generally known as the Stop Band as the input signal is blocked or stopped from passing through.<br>

    The cut-off frequency can be found using the standard formula, ƒc = 1/(2πRC).
 