#### Procedure
1.	Procedure :-

For tension test:
 
Step 1: Make the circuit of low pass filter .

Step 2: Choose the values of resistor and capacitor.

Step 3: Check the power button if it is "off" then make it "on" to enable the circuit.

Step 4: Then display the graph on display section.

Step 5: A question is asked ,enter the answer. if answer is correct then display "correct answer" otherwise back to the asked question.                                                                          

Step 6: Now change the values of resistance and capacitance.

Step 7: Calculate the cut-off frequency by using formula.

Step 8: Compare this value with the value given by user.

Step 9: If answer is matched then it display" answer is correct" otherwise it will display "check the calculation" and then display correct answer.

